package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseNewService;
@Controller
public class CourseNewController {
    @Autowired
    CourseNewService courseNewService;
    @GetMapping("/courseByNumOfStudent/{num}")
    public ResponseEntity getCourseWhichStudentEnrolledMoreThan(@PathVariable int num){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(courseNewService.getCourseWhichStudentEnrolledMoreThan(num)));
    }
}
