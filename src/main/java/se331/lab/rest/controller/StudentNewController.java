package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.StudentNewService;
@Controller
public class StudentNewController {
    @Autowired
    StudentNewService studentNewService;
    @GetMapping("/studentByName/{name}")
    public ResponseEntity getStudentByName(@PathVariable String name){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentNewService.getStudentByNameContains(name)));
    }
    @GetMapping("/studentByAdvisor/{name}")
    public ResponseEntity getStudentByAdvisorName(@PathVariable String name){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentNewService.getStudentWhoseAdvisorNameIs(name)));
    }
}
