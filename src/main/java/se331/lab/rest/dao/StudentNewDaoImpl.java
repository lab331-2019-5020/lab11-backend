package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.StudentRepository;

import java.util.List;
@Repository
public class StudentNewDaoImpl implements StudentNewDao{
    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }

}
