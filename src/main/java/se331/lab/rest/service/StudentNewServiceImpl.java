package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.StudentNewDao;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;
@Service
public class StudentNewServiceImpl implements StudentNewService {
    @Autowired
    StudentNewDao studentNewDao;

    @Override
    public List<Student> getStudentByNameContains(String PoN) {
        List<Student> students = studentNewDao.getAllStudent();
        List<Student> output = new ArrayList<>();
        for (Student student : students) {
            if (student.getName().contains(PoN)) {
                output.add(student);
            }

        }
        return output;
    }

    @Override
    public List<Student> getStudentWhoseAdvisorNameIs(String advisorName) {
        List<Student> students = studentNewDao.getAllStudent();
        List<Student> output = new ArrayList<>();
        for (Student student : students) {
            String adname = student.getAdvisor().getName();
            if (adname.equalsIgnoreCase(advisorName)) {
                output.add(student);
            }
        }
        return output;
    }
}
