package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseDao;
import se331.lab.rest.entity.Course;

import java.util.ArrayList;
import java.util.List;
@Service
public class CourseNewServiceImpl implements CourseNewService{
    @Autowired
    CourseDao courseDao;
    @Override
    public List<Course> getCourseWhichStudentEnrolledMoreThan(int amountOfStudent) {
        List<Course> courses = courseDao.getAllCourses();
        List<Course> output = new ArrayList<>();
        for (Course course: courses){
            int numstudent = course.getStudents().size();
            if (numstudent > amountOfStudent){
                output.add(course);
            }
        }
        return output;
    }
}
