package se331.lab.rest.service;

import se331.lab.rest.entity.Student;

import java.util.List;

public interface StudentNewService {
    List<Student> getStudentByNameContains(String PoN);
    List<Student> getStudentWhoseAdvisorNameIs(String advisorName);
}
